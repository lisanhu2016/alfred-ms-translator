mod services;
use services::translator::TranslatorService;

use crate::services::translator::{DetectLanguageQuery, LookupQuery, TranslateQuery};

const SECRET_KEY: &'static str = "d9e3696843284277b42e36e930953937";


fn main() {
    let args = parse();
    run(&args);
}

fn run(args: &Args) {
    let mut items = json::array![];
    // word_lookup(&args.text, &mut items);
    // translate_sentence(&args.text, &mut items);

    //  Detect Language
    let mut service = TranslatorService::new();
    let query = DetectLanguageQuery {
        text: args.text.clone(),
    };
    service.set_url("https://api.cognitive.microsofttranslator.com/detect?api-version=3.0");
    service.set_key(SECRET_KEY);
    service.add_query(query);
    let res = service.execute()[0]["language"].as_str().unwrap().to_owned();
    let from = &res;
    let to = if res == "zh-Hans" { "en" } else { "zh-Hans" };
    eprintln!("text:{}\nfrom:{} to:{}\n", &args.text, &from, &to);

    //  Dictionary Lookup
    let mut service = TranslatorService::new();
    let query = LookupQuery {
        text: args.text.clone(),
    };
    service.set_url(
        &format!("https://api.cognitive.microsofttranslator.com/dictionary/lookup?api-version=3.0&from={}&to={}", 
            &from, 
            &to)
    );
    service.set_key(SECRET_KEY);
    service.add_query(query);
    let res = service.execute();
    for trans in res[0]["translations"].members() {
        let title = trans["displayTarget"].as_str().to_owned();
        let obj = json::object! {
            title: title,
            subtitle: trans["posTag"].as_str().to_owned(),
            arg: title,
        };
        items.push(obj).unwrap();
    }

    //  Sentence Lookup
    let mut service = TranslatorService::new();
    let query = TranslateQuery {
        text: args.text.clone(),
    };
    service.set_url(
        &format!("https://api.cognitive.microsofttranslator.com/translate?api-version=3.0&from={}&to={}", 
            &from, 
            &to)
    );
    service.set_key(SECRET_KEY);
    service.add_query(query);
    let res = service.execute();
    for trans in res[0]["translations"].members() {
        let title = trans["text"].as_str().to_owned();
        let obj = json::object! {
            title: title,
            subtitle: from.to_owned(),
            arg: title,
        };
        items.push(obj).unwrap();
    }

    let output = json::object! {
        items: items
    };
    println!("{}", output.dump());
}

fn parse() -> Args {
    use unicode_normalization::UnicodeNormalization;
    use clap::{App, Arg};
    let matches = App::new("Translator using Azure Text API")
        .arg(Arg::with_name("text").required(true))
        .get_matches();
    let text = matches.value_of("text").unwrap().to_owned();
    let text = (&text).nfc().collect();
    Args { text }
}

struct Args {
    text: String,
}
