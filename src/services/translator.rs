use json::{array, object, JsonValue};

pub trait TranslatorRequest {
    fn convert(&self) -> JsonValue;
}

pub struct DetectLanguageQuery {
    pub text: String,
}

pub struct TranslateQuery {
    pub text: String,
}

pub struct LookupQuery {
    pub text: String,
}

impl TranslatorRequest for DetectLanguageQuery {
    fn convert(&self) -> JsonValue {
        object! {
            Text: self.text.as_str()
        }
    }
}

impl TranslatorRequest for TranslateQuery {
    fn convert(&self) -> JsonValue {
        object! {
            Text: self.text.as_str()
        }
    }
}

impl TranslatorRequest for LookupQuery {
    fn convert(&self) -> JsonValue {
        object! {
            Text: self.text.as_str()
        }
    }
}

pub struct TranslatorService {
    requests: JsonValue,
    url: Option<String>,
    key: Option<String>
}

impl TranslatorService {
    pub fn new() -> TranslatorService {
        TranslatorService { requests: array![], url: None, key: None }
    }

    pub fn set_url(&mut self, to: &str) {
        self.url = Some(to.to_owned());
    }

    pub fn set_key(&mut self, key: &str) {
        self.key = Some(key.to_owned());
    }

    fn get_url(&self) -> String {
        self.url.clone().unwrap()
    }

    fn get_key(&self) -> String {
        self.key.clone().unwrap()
    }

    pub fn add_query<Q: TranslatorRequest>(&mut self, query: Q) {
        self.requests.push(query.convert()).unwrap();
    }
    pub fn execute(&self) -> JsonValue {
        let client = reqwest::blocking::Client::new();
        let res = client
            .post(&self.get_url())
            .body(self.requests.to_string())
            .header(
                "Ocp-Apim-Subscription-Key",
                self.get_key(),
            )
            .header("Content-Type", "application/json");
        let res = res.send().unwrap().text().unwrap();
        let parsed = json::parse(&res).unwrap();
        parsed
    }
}
